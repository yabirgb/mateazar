from django.urls import path

from .views import (HomeView, DisplayView)

urlpatterns = [
    path('', HomeView.as_view()),
    path('display', DisplayView.as_view()),
]
