from django import forms

TIPOS_PRESENTACION = (
    ("D", "Dinámica"),
    ("E", "Estática"),
    ("T", "Todos"),
)

class BusquedaForm(forms.Form):
    nivel = forms.IntegerField(min_value = 0, max_value = 5)
    tipo = forms.ChoiceField(choices = TIPOS_PRESENTACION)
    preguntas = forms.IntegerField(min_value = 1)
