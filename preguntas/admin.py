from django.contrib import admin
from .models import (Tema, Pregunta)
# Register your models here.

admin.site.register(Tema)
admin.site.register(Pregunta)
