import random 

from django.shortcuts import render
from django.views.generic import TemplateView
from django.db.models import Q
from django.core import serializers
from django.contrib.messages import get_messages, add_message, INFO
from django.http import HttpResponseRedirect
from django.db.models import Avg, Max, Min

from .models import (Pregunta, Coleccion)
from .forms import BusquedaForm
# Create your views here.
class HomeView(TemplateView):
    template_name = "home.html"

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        form = BusquedaForm(request.POST)
        if form.is_valid():

            #If the form is correct we perform a selection
            data = form.cleaned_data
            preg = data['preguntas']

            if data['nivel'] == 0 and data['tipo'] == 'T':
                preguntas = Pregunta.objects.all()
            elif data['nivel'] == 0 and data['tipo'] != 'T':
                preguntas = Pregunta.objects.filter(presentacion=data['tipo'])
            elif data['nivel'] != 0 and data['tipo'] == 'T':
                preguntas  = Pregunta.objects.filter(nivel = data['nivel'])
            else:
                preguntas = Pregunta.objects.filter(presentacion=data['tipo'], nivel = data['nivel'])
            #print(form.cleaned_data)

            id_s = preguntas.values('solucion').distinct().values_list('id', flat=True)
            c = list(Pregunta.objects.filter(id__in=id_s))
            
            resultado = random.sample(c, min(preg,Pregunta.objects.filter(id__in=id_s).count()))

            result = serializers.serialize('json', resultado)
            add_message(request, INFO, result)
            return HttpResponseRedirect("/display")
        else:
            print("NOT VALID <===")

        return super(TemplateView, self).render_to_response(context)

class DisplayView(TemplateView):
    template_name = "display.html"


    def get_context_data(self, **kwargs):
        context = super(TemplateView, self).get_context_data(**kwargs)
        storage = get_messages(self.request)
        for message in storage:
            context["data"] = message
            break

        return context
