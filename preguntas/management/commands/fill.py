import random

from django.core.files import File
from django.core.management.base import BaseCommand

from preguntas.manage_data import longitud_video

from preguntas.models import (Pregunta, Tema, Coleccion, TIPOS_PRESENTACION)


#https://stackoverflow.com/questions/3844430/how-to-get-the-duration-of-a-video-in-python
# Get or none Tema.objects.filter(name="baby").first()

categorias = ("Matemáticas", "Biología", "Geología", "Fisioterapia",
"Medicina", "Matemáticas/Álgebra", "Matemáticas/Geometría")

path = "/home/yabir/Downloads/Olympea.mp4"

class Command(BaseCommand):

    def _generate(self,number):

        for i in range(number):

            cat = random.choice(categorias)

            p = Pregunta(
            nivel = random.randint(1,5),
            categoria = Tema.objects.get_or_create(nombre=cat)[0],
            duracion = longitud_video(path),
            solucion = random.randint(0,99),
            presentacion = random.choice(TIPOS_PRESENTACION)[0],
            video = File(open(path, 'rb'), 'rb')
            )
            p.save()
            print("Saved {}".format(p))

        print("Preguntas generadas aleatoriamente.")

    def add_arguments(self, parser):
        parser.add_argument('number', type=int)

    def handle(self, *args, **options):
        self._generate(options["number"])
            #self.generate(args)
