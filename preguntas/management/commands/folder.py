import os

from django.core.files import File
from django.core.management.base import BaseCommand

from preguntas.manage_data import longitud_video

from preguntas.models import (Pregunta, Tema, Coleccion, TIPOS_PRESENTACION)


categoria = "Matemáticas"


class Command(BaseCommand):

    def _generate(self, path):
        try:
            files = os.listdir(path)
        except:
            print("Wrong path")

        for video in files:
            name = os.path.splitext(video)[1]

            solution = name[1:]

            p = Pregunta(
                nivel=1,
                categoria = Tema.objects.get_or_create(nombre = categoria)[0],
                duracion = longitud_video(path),
                solucion = solution,
                presentacion = TIPOS_PRESENTACION[0][0],
                video = File(open(os.path.join(path, video), 'rb'), 'rb')
                
            )

            p.save()
            print("Created {}".format(name))
            
    def add_argument(self, parser):
        parser.add_argument('path')

    def handle(self, *args, **options):
        self._generate(options['paht'])
