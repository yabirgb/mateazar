from django.db import models

TIPOS_PRESENTACION = (
    ("D", "Dinámica"),
    ("E", "Estática"),
)

class Tema(models.Model):
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

# Create your models here.
class Pregunta(models.Model):
    nivel = models.IntegerField()
    categoria = models.ForeignKey(Tema, on_delete=models.SET_NULL, null=True)
    duracion = models.IntegerField()
    solucion = models.IntegerField()
    presentacion = models.CharField(max_length=1, choices=TIPOS_PRESENTACION)
    video = models.FileField(upload_to = 'videos/', max_length=500)

    def __str__(self):
        return "{} - {} - {}".format(self.nivel, self.categoria, self.solucion)

class Coleccion(models.Model):
    perguntas = models.ManyToManyField(Pregunta)
    created = models.DateTimeField(editable=False)
