import os

from mutagen.mp4 import MP4

def longitud_video(path):
    audio = MP4(path)
    return audio.info.length
